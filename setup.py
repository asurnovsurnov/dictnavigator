from setuptools import setup, find_packages


setup(
    name='dictnavigator',
    version='1.0',
    packages=['dictnavigator'],
    url='https://gitlab.com/asurnovsurnov/dictnavigetor.git',
    author='Aleksei Surnov',
    author_email='asurnovsurnov@gmail.com',
    description='dictnavigator is a tool for working with nested dictionaries in Python.'
                'The main DictNavigatorKey class provides convenience methods for retrieving, updating, '
                'and deleting values for given keys at any dictionary depth, '
                'even if the key:value pair is contained in another nested data type (tuple, list, dictionary). '
                'In addition, it allows you to get a list of all keys in the dictionary at any nesting level.'
                'This library will come in handy if you work with data in JSON format, especially if you receive it from an API. '
                'It simplifies working with this data, allowing you to quickly retrieve the values you need and update them.',
    python_requires='>=3.5.2',
    license='MIT License'
)
