import unittest
from dictnavigator.navigator import DictNavigatorKey
from .data_test import example_dict, del_example_dict, update_example_dict, all_list_keys, extract_value


class DictNavigatorKeyTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.dnk = DictNavigatorKey(example_dict)

    def test_get_dict(self):
        self.assertEqual(self.dnk.get_dict(), example_dict)

    def test_extract_all_keys(self):
        self.assertEqual(list(sorted(self.dnk.extract_all_keys())), all_list_keys)

    def test_extract_keys_values(self):
        self.assertEqual(self.dnk.extract_keys_values("k3"), extract_value)

    def test_delete_key(self):
        self.dnk.delete_key("k4", "v4")
        self.assertEqual(self.dnk.get_dict(), del_example_dict)

    def test_update_value(self):
        self.dnk.update_value("k3", ["v_list3", "v_list4"], ["Artem", "Maksim"])
        self.assertEqual(self.dnk.get_dict(), update_example_dict)



if __name__ == '__main__':
    unittest.main()
