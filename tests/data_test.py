
example_dict = {"k1": "v1",
                "k2": {"k3": ["v_list3", "v_list4"],
                       "k4": "v_string1",
                       "k5": [{"k1", "dictv1"}]
                       },
                "k3": ({"k_tuple1": "v_tuple1"}, {"k_tuple2": "v_tuple2"}),
                "k4": "v4"}

del_example_dict = {"k1": "v1",
                "k2": {"k3": ["v_list3", "v_list4"],
                       "k4": "v_string1",
                       "k5": [{"k1", "dictv1"}]
                       },
                "k3": ({"k_tuple1": "v_tuple1"}, {"k_tuple2": "v_tuple2"})}

update_example_dict = {"k1": "v1",
                       "k2": {"k3": ["Artem", "Maksim"],
                              "k4": "v_string1",
                              "k5": [{"k1", "dictv1"}]
                       },
                       "k3": ({"k_tuple1": "v_tuple1"}, {"k_tuple2": "v_tuple2"})}

extract_value = [{"k3": ["v_list3", "v_list4"]}, {"k3": ({"k_tuple1": "v_tuple1"}, {"k_tuple2": "v_tuple2"})}]

all_list_keys = ['k1', 'k2', 'k3', 'k4', 'k5', 'k_tuple1', 'k_tuple2']

